from django.contrib import admin

from apps.market.models import ShoppingCenter, Space, Category, Tag, Photo


@admin.register(ShoppingCenter)
class ShoppingCenterAdmin(admin.ModelAdmin):
    list_display = ['name', 'address']


class PhotoInline(admin.TabularInline):
    model = Photo
    extra = 1


@admin.register(Space)
class SpaceAdmin(admin.ModelAdmin):
    inlines = [PhotoInline]
    list_display = ['name', 'state']


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name']


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    list_display = ['name']





