from django.urls import path
from .api import views

urlpatterns = [
    path('shopping-center/list', views.ShoppingCenterList.as_view(), name='shopping_center_list'),
    path('shopping-center/<int:pk>/detail', views.ShoppingCenterList.as_view(), name='shopping_center_list'),
    path('space/list', views.SpaceList.as_view(), name='space_list'),
    path('user/space/list', views.UserSpaceList.as_view(), name='user_space_list'),
    path('space/<int:pk>/detail', views.SpaceDetail.as_view(), name='space_detail'),
    path('category/list', views.CategoryList.as_view(), name='category_list'),
]