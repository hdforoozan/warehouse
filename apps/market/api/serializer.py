from rest_framework import serializers
from apps.market.models import ShoppingCenter, Space, Category, Tag, Photo


class TagsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = [
            'id',
            'name'
        ]


class PhotoSpaceSerializer(serializers.ModelSerializer):

    class Meta:
        model = Photo
        fields = ['id', 'photo']


class SpaceSerializerList(serializers.ModelSerializer):
    state = serializers.CharField(source='get_state_display')
    category = serializers.CharField(source='category.name', default=None)
    photos = PhotoSpaceSerializer(many=True, read_only=True)

    class Meta:
        model = Space
        fields = ['id', 'name', 'state', 'price_per_day', 'price_per_month', 'price_per_three_month',
                  'price_per_year', 'shopping_center', 'category', 'photos']


class SpaceSerializerDetail(serializers.ModelSerializer):
    state = serializers.CharField(source='get_state_display')
    tags = TagsSerializer(many=True, read_only=True)
    category = serializers.CharField(source='category.name', default=None)
    photos = PhotoSpaceSerializer(many=True, read_only=True)
    similar_spaces = serializers.SerializerMethodField()

    class Meta:
        model = Space
        fields = ['id', 'name', 'state', 'dimensions', 'price_per_day', 'price_per_month', 'price_per_three_month',
                  'price_per_year', 'description', 'shopping_center', 'category', 'tags', 'photos', 'similar_spaces']

    def get_similar_spaces(self, space):
        return SpaceSerializerList(Space.objects.exclude(pk=space.pk).filter(tags__space=space), many=True).data


class CategorySerializerList(serializers.ModelSerializer):

    class Meta:
        model = Category
        fields = ['id', 'name']


class ShoppingCenterSerializer(serializers.ModelSerializer):
    space = SpaceSerializerList(many=True, read_only=True)

    class Meta:
        model = ShoppingCenter
        fields = ['name', 'space']


class SpaceSerializer(serializers.ModelSerializer):
    category = serializers.PrimaryKeyRelatedField(required=False, queryset=Category.objects.all())
    photos = PhotoSpaceSerializer(required=False, many=True, read_only=True)

    class Meta:
        model = Space
        fields = ['name', 'state', 'dimensions', 'price_per_day', 'price_per_month', 'price_per_three_month',
                  'price_per_year', 'description', 'category', 'tags', 'photos']
