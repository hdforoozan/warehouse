from apps.market.models import ShoppingCenter, Space, Category


class MarketRepository:
    def getShoppingCenterListRepo(self):
        return ShoppingCenter.objects.all().order_by('id')

    def getSpaceListRepo(self, user=None):
        if user:
            return Space.objects.filter(user=user).order_by('id')
        return Space.objects.all().order_by('id')

    def getSpaceRepo(self, pk):
        return Space.objects.filter(pk=pk).first()

    def getCategoryListRepo(self):
        return Category.objects.all().order_by('id')
