from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from Warehouse_Backend.custom_pagination import paginator
from Warehouse_Backend.dynamic_filter import DynamicFilter
from apps.market.api.serializer import ShoppingCenterSerializer, SpaceSerializer, SpaceSerializerList, \
    CategorySerializerList, SpaceSerializerDetail
from apps.market.api.service import MarketService
from django.utils.translation import gettext as _

from apps.market.models import Space


class ShoppingCenterList(APIView, MarketService):
    def get(self, request):
        shopping_centers = self.getShoppingCenterList()
        shopping_centers = DynamicFilter(request.GET, shopping_centers).evaluate()
        result_shopping_centers = paginator.paginate_queryset(shopping_centers, request)
        serializer = ShoppingCenterSerializer(result_shopping_centers, many=True)
        return paginator.get_paginated_response(serializer.data)


class SpaceList(APIView, MarketService):
    def get(self, request):
        spaces = self.getSpaceList()
        spaces = DynamicFilter(request.GET, spaces).evaluate()
        result_spaces = paginator.paginate_queryset(spaces, request)
        serializer = SpaceSerializerList(result_spaces, many=True)
        return paginator.get_paginated_response(serializer.data)

    def post(self, request):
        serializer = SpaceSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(user=request.user)
            return Response({
                'success': True,
                'message': _('mission accomplished'),
                'data': serializer.data
            }, status=status.HTTP_201_CREATED)
        return Response(
            {'success': False,
             'message': _('Inserted data is wrong in some fields'),
             'data': {'messages': serializer.errors}
             },
            status.HTTP_400_BAD_REQUEST)


class UserSpaceList(APIView, MarketService):
    def get(self, request):
        spaces = self.getSpaceList(user=request.user)
        spaces = DynamicFilter(request.GET, spaces).evaluate()
        result_spaces = paginator.paginate_queryset(spaces, request)
        serializer = SpaceSerializerList(result_spaces, many=True)
        return paginator.get_paginated_response(serializer.data)


class CategoryList(APIView, MarketService):
    def get(self, request):
        categories = self.getCategoryList()
        categories = DynamicFilter(request.GET, categories).evaluate()
        result_categories = paginator.paginate_queryset(categories, request)
        serializer = CategorySerializerList(result_categories, many=True)
        return paginator.get_paginated_response(serializer.data)


class SpaceDetail(APIView, MarketService):
    def get(self, request, pk):
        space = self.getSpace(pk=pk)
        if space:
            serializer = SpaceSerializerDetail(instance=space, many=False)
            return Response({
                "success": True,
                "message": _('mission accomplished'),
                "data": serializer.data
            })
        return Response({
            'success': False,
            'message': _('not found')
        }, status=status.HTTP_404_NOT_FOUND)

    def put(self, request, pk):
        space = Space.objects.filter(user=request.user, pk=pk).first()
        if space:
            serializer = SpaceSerializer(instance=space, data=request.data)
            if serializer.is_valid():
                serializer.save(user=request.user)
                return Response({
                    'success': True,
                    'message': _('mission accomplished'),
                    'data': serializer.data
                }, status=status.HTTP_200_OK)
        return Response({
            'success': False,
            'message': _('not found')
        }, status=status.HTTP_404_NOT_FOUND)

