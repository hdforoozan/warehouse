from apps.market.api.repository import MarketRepository


class MarketService(MarketRepository):
    def getShoppingCenterList(self):
        return self.getShoppingCenterListRepo()

    def getSpaceList(self, user=None):
        return self.getSpaceListRepo(user=user)

    def getSpace(self, pk):
        return self.getSpaceRepo(pk=pk)

    def getCategoryList(self):
        return self.getCategoryListRepo()
