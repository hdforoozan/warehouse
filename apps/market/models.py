from django.db import models
from django.utils.translation import gettext as _

from apps.user.models import User


class SpaceState(models.IntegerChoices):
    IN_SELL = 0, _('IN_SELL')
    SOLD = 1, _('SOLD')
    IN_FUTURE = 2, _('IN_FUTURE')


class ShoppingCenter(models.Model):
    name = models.CharField(max_length=255)
    address = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.name


class Category(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Tag(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Space(models.Model):
    name = models.CharField(max_length=255)
    shopping_center = models.ForeignKey(ShoppingCenter, on_delete=models.SET_NULL, null=True, blank=True,
                                        related_name='space')
    state = models.IntegerField(choices=SpaceState.choices, null=True, blank=True)
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True, related_name='space_user')
    category = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True, blank=True)
    dimensions = models.IntegerField(blank=True, null=True)
    price_per_day = models.DecimalField(max_digits=16, decimal_places=0, null=True, blank=True)
    price_per_month = models.DecimalField(max_digits=16, decimal_places=0, null=True, blank=True)
    price_per_three_month = models.DecimalField(max_digits=16, decimal_places=0, null=True, blank=True)
    price_per_year = models.DecimalField(max_digits=16, decimal_places=0, null=True, blank=True)
    tags = models.ManyToManyField(Tag, blank=True)
    description = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.name


class Photo(models.Model):
    space = models.ForeignKey(Space, on_delete=models.SET_NULL, null=True, blank=True, related_name='photos')
    photo = models.ImageField(upload_to='space/', null=True, blank=True)

