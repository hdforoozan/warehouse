import re
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import Group
from django.core.validators import MinLengthValidator
from django.db.models import Q
from rest_framework.validators import UniqueValidator
from rest_framework import serializers
from password_strength import PasswordPolicy
from django.utils.translation import gettext as _
from apps.user.api.service import mobileValidation
from apps.user.models import User, CompanyProfile


class LoginSerializer(serializers.ModelSerializer):
    user = serializers.CharField(required=True, min_length=1, max_length=255)

    class Meta:
        model = User
        fields = ['user']


class MobileSerializer(serializers.ModelSerializer):
    mobile = serializers.CharField(required=True, min_length=11, max_length=13)

    class Meta:
        model = User
        fields = ['mobile']

    def validate_mobile(self, value):
        if not mobileValidation(value):
            raise serializers.ValidationError(_("validation error"))
        return value


class LoginOtpSerializer(serializers.ModelSerializer):
    mobile = serializers.CharField(required=True, min_length=11, max_length=13)
    otp = serializers.CharField(required=False, min_length=4, max_length=4)

    class Meta:
        model = User
        fields = ['mobile', 'otp']

    def validate_mobile(self, value):
        if not mobileValidation(value):
            raise serializers.ValidationError(_("validation error"))
        return value


class PasswordSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True, required=False, min_length=5, max_length=30)
    confirm_password = serializers.CharField(write_only=True, required=False)
    otp = serializers.CharField(required=True, min_length=4, max_length=4)

    class Meta:
        model = User
        fields = ['password', 'confirm_password', 'otp']

    def validate_password(self, value):
        policy = PasswordPolicy.from_names(
            numbers=1,
        )
        test = policy.test(value)
        if test != []:
            raise serializers.ValidationError(test)
        return value

    def validate_confirm_password(self, password):
        if self.initial_data['password'] != password:
            raise serializers.ValidationError(_('password does not match confirmation'))
        return password

    def update(self, instance, validated_data):
        instance.password = make_password(validated_data['password'])
        instance.save()
        return instance


class UserSerializer(serializers.ModelSerializer):
    groups = serializers.SlugRelatedField(
        many=True,
        read_only=True,
        slug_field='name',
    )
    mobile = serializers.CharField(required=False)
    date_joined = serializers.ReadOnlyField()
    password = serializers.CharField(write_only=True, required=False, min_length=5, max_length=30)
    confirm_password = serializers.CharField(write_only=True, required=False)
    first_name = serializers.CharField(max_length=200, required=False)
    last_name = serializers.CharField(max_length=200, required=False)
    email = serializers.EmailField(required=False)
    national_id = serializers.CharField(max_length=255, required=False, allow_blank=True)

    class Meta:
        model = User
        fields = ['mobile', 'first_name', 'last_name', 'email', 'password',
                  'confirm_password', 'national_id', 'groups', 'date_joined']

    def validate_email(self, email):
        if User.objects.filter(Q(email__exact=email) & ~Q(mobile=self.initial_data['mobile'])).count() > 0:
            raise serializers.ValidationError(_('Universally unique identifier'))
        return email

    def validate_confirm_password(self, password):
        if self.initial_data['password'] != password:
            raise serializers.ValidationError(_('password does not match confirmation'))
        return password

    def update(self, instance, validated_data):
        if 'password' in validated_data:
            instance.password = make_password(validated_data['password'])
        if instance.email is None or instance.email == '':
            instance.email = validated_data.get('email')
        instance.first_name = validated_data.get('first_name', instance.first_name)
        instance.last_name = validated_data.get('last_name', instance.last_name)
        instance.national_id = validated_data.get('national_id', instance.national_id)
        instance.save()
        return instance


class CompanyProfileSerializer(serializers.ModelSerializer):
    user_id = serializers.IntegerField(required=True, validators=[UniqueValidator(queryset=CompanyProfile.objects.all())])
    name = serializers.CharField(required=True, max_length=255)
    national_id = serializers.CharField(required=True, max_length=255)

    class Meta:
        model = CompanyProfile
        exclude = ['user']



