from apps.user.models import User, CompanyProfile


class UserRepository:
    def getUserRepo(self, mobile):
        return User.objects.filter(mobile=mobile).first()

    def getCompanyProfileRepo(self, user):
        return CompanyProfile.objects.filter(user=user).first()
