import base64
import re
import uuid

import kavenegar
from django.core.files.base import ContentFile
import secrets
from django.core.cache import cache
# import kavenegar
import hashlib
# from mail_templated import send_mail
# from apps.notifications.api.smsdriver import SMSDriver
# from xaas_web_backend.settings import KAVENEGAR_TOKEN, ACTIVE_SMS
# from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework_simplejwt.tokens import RefreshToken

from Warehouse_Backend.settings import ACTIVE_SMS, KAVENEGAR_TOKEN
from apps.user.api.repository import UserRepository


def mobileValidation(value):
    pattern = re.compile("(0|\+98)?([ ]|-|[()]){0,2}9[0|1|2|3|4|9]([ ]|-|[()]){0,2}(?:[0-9]([ ]|-|[()]){0,2}){8}")
    return bool(pattern.match(value))


def imageSize(b64string):
    return (len(b64string) * 3) / 4 - b64string.count('=', -2)


def imageConvertor(base_64_image):
    base64_file = base_64_image
    image_size = imageSize(base_64_image)
    format, imgstr = base64_file.split(';base64,')
    ext = format.split('/')[-1]
    name = str(uuid.uuid4())
    data = ContentFile(base64.b64decode(imgstr), name=name + "." + ext)
    return data, ext, name, image_size


class UserService(UserRepository):
    def getToken(self, user):
        refresh = RefreshToken.for_user(user)
        return {
            'refresh': str(refresh),
            'access': str(refresh.access_token),
        }

    def generate(self, request, ttl, template, extra_argument=None):
        secretsGenerator = secrets.SystemRandom()
        data = secretsGenerator.randint(1111, 9999)
        if extra_argument is None:
            cache.set("{}{}".format(template, request.data.get('mobile', request.user)),
                      hashlib.md5(str(data).encode()).hexdigest(), timeout=ttl)
        else:
            cache.set("{}{}{}".format(template, request.data.get('mobile', request.user), extra_argument),
                      hashlib.md5(str(data).encode()).hexdigest(), timeout=ttl)

        if 'throttle' in template:
            return None

        # if ACTIVE_SMS == 'KAVENEGAR_TOKEN':
        # api = kavenegar.KavenegarAPI(KAVENEGAR_TOKEN)
        # else:
        #     api = SMSDriver()
        print(data)
        # params = {
        #     'receptor': request.data.get('mobile', request.user),
        #     'template': template,
        #     'token': data,
        #     'type': 'sms',
        # }
        # response = api.verify_lookup(params)

    def customThrottling(self, request, ttl, template):
        cache.set("{}{}".format(template, request.data.get('mobile', request.user)),
                  hashlib.md5('throttling'.encode()).hexdigest(), timeout=ttl)

    def getTTL(self, request, template, extra_argument=None):
        if extra_argument is not None:
            return cache.ttl("{}{}{}".format(template, request.data.get('mobile', request.user), extra_argument))
        return cache.ttl("{}{}".format(template, request.data.get('mobile', request.user)))

    def getValue(self, request, template, extra_argument=None):
        if extra_argument is None:
            token = cache.get("{}{}".format(template, request.data.get('mobile', request.user)))
        else:
            token = cache.get("{}{}{}".format(template, request.data.get('mobile', request.user), extra_argument))
        return token

    def deleteValue(self, request, template, extra_argument=None):
        if extra_argument is not None:
            cache.delete("{}{}{}".format(template, request.data.get('mobile', request.user), extra_argument))
        else:
            cache.delete("{}{}".format(template, request.data.get('mobile', request.user)))

    def getUser(self, mobile):
        return self.getUserRepo(mobile=mobile)

    def getCompanyProfile(self, user):
        return self.getCompanyProfileRepo(user=user)
