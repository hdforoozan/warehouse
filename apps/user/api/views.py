import hashlib
from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import permission_classes
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny
from rest_framework.throttling import UserRateThrottle, AnonRateThrottle
from django.db.models import Q
from django.core.cache import cache
from django.utils.translation import gettext as _

# class GetUserRateThrottle(UserRateThrottle):
#     rate = '3/minute'

from apps.user.api.serializer import MobileSerializer, LoginOtpSerializer, UserSerializer, LoginSerializer, \
    PasswordSerializer, CompanyProfileSerializer
from apps.user.api.service import UserService
from apps.user.models import User


class OTPPasswordThrottle(UserRateThrottle):
    rate = '3/minute'


class OTPRateThrottle(UserRateThrottle):
    rate = '3/minute'


class OTPLoginRateThrottle(UserRateThrottle):
    rate = '4/minute'


@permission_classes((AllowAny,))
class SendOtp(APIView, UserService):
    throttle_classes = [OTPRateThrottle, AnonRateThrottle]

    def post(self, request):
        serializer = MobileSerializer(data=request.data)
        if serializer.is_valid():
            request.data['mobile'] = "0" + request.data['mobile'][-10:]

            if self.getTTL(request, 'otp') == 0:
                ttl = 120
                self.generate(request, ttl, 'otp')
            else:
                ttl = cache.ttl("otp{}".format(request.data['mobile']))
            return Response(
                {'success': True,
                 'message': _('your code was sent successfully'),
                 'dev_message': 'OTP SEND',
                 'data': {'ttl': ttl}})
        else:
            return Response(
                {'success': False,
                 'message': _('your number is not correct'),
                 'data': {'messages': serializer.errors}},
                status.HTTP_400_BAD_REQUEST)


@permission_classes((AllowAny,))
class LoginByOtp(APIView, UserService):
    throttle_classes = [OTPLoginRateThrottle, AnonRateThrottle]

    def post(self, request):
        serializer = LoginOtpSerializer(data=request.data)
        if serializer.is_valid():
            request.data['mobile'] = "0" + request.data['mobile'][-10:]
            if self.getTTL(request, 'otp') == 0:
                return Response(
                    {
                        'success': False, 'message': _('your time is up'),
                        'dev_message': 'otp expired'

                    }, status=status.HTTP_404_NOT_FOUND)
            else:
                if self.getValue(request, 'otp') == hashlib.md5(str(request.data['otp']).encode()).hexdigest():
                    self.deleteValue(request, 'otp')
                    user = User.objects.filter(mobile__exact=request.data['mobile']).first()
                    new_user = False
                    if user is None:
                        new_user = True
                        user = User.objects.create_user(request.data['mobile'])

                    token = self.getToken(user)
                    return Response({
                        'success': True,
                        'data': {'token': token,
                                 'new_user': new_user,
                                 'user_info': UserSerializer(user, many=False).data,
                                 },
                        'message': _("welcome to Warehouse"),
                        'dev_message': 'token generate'
                    })
                else:
                    return Response(
                        {"success": False,
                         "message": _("Code is not correct"),
                         'dev_message': 'incorrect otp'},
                        status.HTTP_400_BAD_REQUEST)
        else:
            return Response(
                {'success': False,
                 'message': _('something is wrong'),
                 'data': {'messages': serializer.errors}},
                status.HTTP_400_BAD_REQUEST)


@permission_classes((AllowAny,))
class StaticLogin(APIView, UserService):
    # throttle_classes = [GetUserRateThrottle, AnonRateThrottle]
    def post(self, request):
        login_serializer = LoginSerializer(data=request.data)
        if not login_serializer.is_valid():
            return Response({'success': False,
                             'dev_message': 'wrong functionality',
                             'message': _('your information is not correct'),
                             'data': {'messages': login_serializer.errors}},
                            status=status.HTTP_400_BAD_REQUEST)
        user = User.objects.filter(Q(mobile__exact=request.data['user']) |
                                   Q(email__exact=request.data['user'])).first()
        if user is None:
            return Response({
                'success': False,
                'message': _('user not found'),
                'dev_message': 'not found'
            }, status=status.HTTP_404_NOT_FOUND)
        if user.check_password(request.data['password']):
            token = self.getToken(user)
            return Response({
                'success': True,
                'data': {
                    'token': token,
                    'user_info': UserSerializer(user, many=False).data
                },
                'message': _('welcome to XaaSCloud'),
                'dev_message': 'token generate'
            })
        else:
            return Response({
                'success': False,
                'message': _("your information is not correct"),
                'dev_message': 'mistake user and pass'
            }, status=status.HTTP_401_UNAUTHORIZED)


class UserList(APIView):
    def get(self, request):
        serializer = UserSerializer(instance=request.user)
        return Response({'success': True,
                         'message': _('mission accomplished'),
                         'dev_message': 'done',
                         'data': serializer.data})

    def put(self, request):
        request.data['mobile'] = request.user.mobile
        serializer = UserSerializer(data=request.data, instance=request.user)
        if serializer.is_valid():
            serializer.save()
            return Response({'success': True,
                             'message': _('Data entry was successful'),
                             'dev_message': 'done',
                             'data': serializer.data})
        else:
            return Response({'success': False,
                             'dev_message': 'wrong functionality',
                             'message': _('your information is not correct'),
                             'data': {'messages': serializer.errors}},
                            status=status.HTTP_400_BAD_REQUEST)


class SendChangePasswordOTP(UserService, APIView):
    throttle_classes = [OTPPasswordThrottle, AnonRateThrottle]

    def get(self, request):
        self.generate(request, 60, 'functionality', request.user.id)
        return Response({"success": True,
                         "message": _('mission accomplished'),
                         'dev_message': 'done'})


class ChangePassword(UserService, APIView):
    throttle_classes = [OTPPasswordThrottle, AnonRateThrottle]

    def post(self, request):
        serializer = PasswordSerializer(data=request.data, instance=request.user)
        if serializer.is_valid():
            if self.getTTL(request, 'functionality', str(request.user.id)) == 0 or \
                    self.getValue(request, 'functionality', str(request.user.id)) != hashlib.md5(
                str(request.data['otp']).encode()).hexdigest():
                return Response(
                    {
                        'success': False, 'message': _('expire time to live of one-time password'),
                        'dev_message': 'otp expired'

                    }, status=status.HTTP_404_NOT_FOUND)
            serializer.save()
            return Response({"success": True, "message": _('Password Changed'), 'dev_message': 'Password Changed'})
        else:
            return Response({"success": False, 'dev_message': 'wrong functionality'
                                , "message": _('your information is not correct'),
                             'data': {'messages': serializer.errors}},
                            status=status.HTTP_400_BAD_REQUEST)


class CompanyProfile(APIView, UserService):
    def post(self, request):
        request.data['user_id'] = request.user.id
        serializer = CompanyProfileSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'success': True,
                             "message": _('profile projection'),
                             'dev_message': 'done',
                             'data': serializer.data}, status.HTTP_201_CREATED)
        else:
            return Response(
                {'success': False,
                 'message': _('your information is not correct'),
                 'data': {'messages': serializer.errors}},
                status.HTTP_400_BAD_REQUEST)

    def get(self, request):
        company_profile = self.getCompanyProfile(user=request.user)
        serializer = CompanyProfileSerializer(company_profile, many=False)
        return Response({"success": True,
                         "message": _('Legal User Profile'),
                         'dev_message': 'done',
                         'data': serializer.data})

    def put(self, request):
        request.data['user_id'] = request.user.id
        company_profile = self.getCompanyProfile(user=request.user)
        if company_profile:
            serializer = CompanyProfileSerializer(company_profile, data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response({'success': True,
                                 'message': _('Changes was successful'),
                                 'dev_message': 'done',
                                 'data': serializer.data})
            return Response(
                {'success': False,
                 'message': _('your information is not correct'),
                 'data': {'messages': serializer.errors}},
                status.HTTP_400_BAD_REQUEST)

        return Response({'success': False,
                         'message': _('user not found'),
                         'dev_message': 'Not found'},
                        status.HTTP_404_NOT_FOUND)
