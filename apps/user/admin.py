"""Integrate with admin module."""

from django.contrib import admin, messages
from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin
from django.utils.translation import gettext_lazy as _
from rangefilter.filters import DateRangeFilter
from apps.user.models import User, CompanyProfile


@admin.register(User)
class UserAdmin(DjangoUserAdmin):
    """Define admin model for custom User model with no mobile field."""
    actions = ['ListUserDontHaveActiveService', ]
    fieldsets = (
        (None, {'fields': ('mobile', 'password')}),
        (_('Personal info'), {'fields': (
            'first_name', 'last_name', 'email', 'national_id')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    readonly_fields = ['national_id']
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('mobile', 'password1', 'password2'),
        }),
    )
    list_display = ('mobile', 'first_name', 'last_name', 'email', 'is_staff', 'date_joined')
    search_fields = ('mobile', 'first_name', 'last_name', 'email')
    ordering = ('mobile',)


@admin.register(CompanyProfile)
class CompanyProfileAdmin(admin.ModelAdmin):
    list_display = ['user', 'name', 'national_id']
    search_fields = ['user__mobile', 'user__email', 'name', 'national_id']
