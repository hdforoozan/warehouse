from django.urls import path
from .api import views

urlpatterns = [
    path('', views.UserList.as_view(), name='user_list'),
    path('send_change_password', views.SendChangePasswordOTP.as_view(), name='send_change_password'),
    path('change_password', views.ChangePassword.as_view(), name='change_password'),
    path('company/profile', views.CompanyProfile.as_view(), name='user_profile_list'),
    path('send/otp', views.SendOtp.as_view(), name='user_send_otp'),
    path('login/otp', views.LoginByOtp.as_view(), name='user_login_otp'),
    path('login/static', views.StaticLogin.as_view(), name='user_static_login'),
]