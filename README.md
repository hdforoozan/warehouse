## Requirements
- Python (3.10)
- Mysql (optional)
## Installation 
- Create  virtual environment(optional)
- Install the dependencies and devDependencies  
```sh
$ pip install -r requirements.txt
```

- Copy  Warehouse_Backend/settings_production.py to  Warehouse_Backend/settings.py
```sh
$ cp  Warehouse_Backend/settings_production.py Warehouse_Backend/settings.py
```
- Set DataBase Connection in Warehouse_Backend/settings.py
```python
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'test',
        'USER': 'test',
        'PASSWORD': 'test@test',
        'HOST': '127.0.0.1',  # Or an IP Address that your DB is hosted on
        'PORT': '3306',
        'OPTIONS': {
            # Tell MySQLdb to connect with 'utf8mb4' character set
            'charset': 'utf8mb4',
        },
    }
}
```
- Create Table In DataBase   
```sh
$ python manage.py migrate
```
- Create Super User
```sh
$ python manage.py createsuperuser
```
- Run Server and admin route  http://127.0.0.1:8000/admin
```sh
$ python manage.py runserver
```
- Run celery and celery beat  
```sh
$ celery -A Warehouse_Backend worker -l info -B
```
- purge celery task in queue system
```sh
$ celery -A Warehouse_Backend purge
```

## Standard
####Naming in Projects
- application  (Plural => e.x: packages,users,...)
- API VIew Method (request method => e.x: get,post,put,delete,...)
- Class  (PascalCase => e.x User)
- method (camelCase => e.x: userLoginCount )
- property or other variables (snake_case => e.x: user_login_count )
 


License
----

