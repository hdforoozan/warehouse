from rest_framework import pagination
from rest_framework.response import Response
from rest_framework import status


class CustomPagination(pagination.PageNumberPagination):
    # def get_next(self):
    #     if not self.page.has_next():
    #         return None
    #     url = self.request.build_absolute_uri()
    #     page_number = self.page.next_page_number()
    #     return page_number
    #
    # def get_previous(self):
    #     if not self.page.has_previous():
    #         return None
    #     page_number = self.page.previous_page_number()
    #     return page_number
    def get_paginated_response(self, data):
        try:
            return Response({
                'success': True,
                'result': {
                    'count': self.page.paginator.count,
                    'perpage': self.page_size,
                    'next': self.get_next_link(),
                    # 'next': self.get_next(),
                    'current': self.page.number,
                    'previous': self.get_previous_link(),
                    'lastpage': self.page.paginator.num_pages,
                    'data': data
                },
            })
        except Exception:
            return Response({'success': False}, status.HTTP_400_BAD_REQUEST)


paginator = CustomPagination()
