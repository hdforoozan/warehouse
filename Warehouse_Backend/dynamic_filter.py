from django.db.models import Q


class DynamicFilter:
    """give request.GET"""

    def __init__(self, request, model):
        self.request = request
        self.model = model

    def evaluate(self):
        and_condition = Q()
        for key, value in self.request.items():
            if key in ['sort', 'page']:
                continue
            if '__in' in key:
                value = value.spilit(',')
            and_condition.add(Q(**{key: value}), Q.AND)
        self.model = self.model.filter(and_condition)
        if "sort" in self.request:
            self.model = self.model.order_by(*self.request['sort'].split(","))
        return self.model